__author__ = 'Daniel'
import socket
IP_AND_PORT = '127.0.0.1', 55555


def receive_fragments(server_sock):
    """
    server_sock: a UDP server socket.

    receives packets from any client who sends them to server_sock and prints their information.
    """
    while True:
        data, remote_add = server_sock.recvfrom(1024)
        print data


def main():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(IP_AND_PORT)
    receive_fragments(sock)


if __name__ == "__main__":
    main()