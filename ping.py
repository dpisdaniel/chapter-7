__author__ = 'Daniel'
import sys
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
istd, ostd , estd = sys.stdin, sys.stdout, sys.stderr
from scapy.all import *
from scapy.layers.dns import DNS, DNSQR, DNSRR
sys.stdin, sys.stdout, sys.stderr = istd, ostd, estd


def send_icmp_paks(server_ip, packet_amount):
    pak_list = []
    for i in range(packet_amount):
        pak = IP(dst=server_ip)/ICMP(type="echo-request", id=i)/"Shit"
        pak_list.append(pak)
    response = sr(pak_list, timeout=1, verbose=False)
    for pak in response:
        pak.show()
    return len(response)


def main():
    server_ip = sys.argv[1]
    print "Sending %s packets to %s . . ." % (sys.argv[2], server_ip)
    paks_received = send_icmp_paks(server_ip, int(sys.argv[2]))
    print "Received %d Reply packets." % paks_received

if __name__ == "__main__":
    main()