__author__ = 'Daniel'
import sys
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
istd, ostd , estd = sys.stdin, sys.stdout, sys.stderr
from scapy.all import *
from scapy.layers.dns import DNS, DNSQR, DNSRR
sys.stdin, sys.stdout, sys.stderr = istd, ostd, estd
from random import randint
MF_FLAG = "MF"
IP_DST = "170.170.170.170"
DPORT = 55555


class MessageFragmenter():
    """
    This class splits the message to the amount of parts that were requested.
    After splitting the message it can send it to the udp_frag_server server, each message part as a 3rd layer
    fragment.
    """
    def __init__(self, message, parts):
        """
        message: a string containing some message.
        parts: an int that has the amount of parts that message should be split to.

        builds the class, puts each parameter received in a new class variable.
        Also checks that the amount of parts that the message will be split to is a valid amount of parts,
        after that, splits the message to all the message parts and puts the split message in another class variable.
        """
        if len(message) < parts or parts == 0 or parts < 0:
            raise ValueError
        self.message = message
        self.parts = parts
        self.split_msg = self._split_message()

    def _split_message(self):
        """
        Splits our message to self.parts parts with random lengths for each part.
        """
        split_message = []
        index = 0
        while len(split_message) < self.parts - 1:
            available_max_split = len(self.message[index:]) - (self.parts - len(split_message) - 1)
            # Got the maximum length that can be in one part of the message for this part of the message
            random_length = randint(1, available_max_split)
            # Got the actual length that the message will be split to
            split_message.append(self.message[index: index + random_length])
            index += random_length
        split_message.append(self.message[index:])
        return split_message

    def send_message(self):
        """
        fragments the message to the amount of parts that the user requested and sends it to the server.
        """
        fragment_ids = randint(1, 20000)  # Gets a random id for the fragments
        first_fragmented_packet = IP(flags=MF_FLAG, dst=IP_DST, id=fragment_ids) / UDP(dport=DPORT) / Raw(self.split_msg[0])
        send(first_fragmented_packet)
        if len(self.split_msg) > 1:  # No need to continue if the split message length is 1
            for msg_index in range(1, len(self.split_msg) - 1):
                fragmented_packet = IP(flags=MF_FLAG, dst=IP_DST, id=fragment_ids) / self.split_msg[msg_index]
                send(fragmented_packet)
            last_fragmented_packet = IP(dst=IP_DST, id=fragment_ids) / self.split_msg[-1]
            send(last_fragmented_packet)


def main():
    msg = raw_input("Insert the message that you would like to send\n")
    try:
        parts = int(raw_input("Insert the amount of parts that you would like to split your message to\n"))
    except ValueError:
        print "You are stupid"
    sender = MessageFragmenter(msg, parts)
    sender.send_message()


if __name__ == "__main__":
    main()